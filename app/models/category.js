module.exports = function(sequelize, Sequelize) {
    var Category = sequelize.define('category', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        },
        parent: {
            type: Sequelize.STRING
        },
        child: {
            type: Sequelize.STRING
        }
    })
    Category.associate = function(models) {
        Category.hasMany(models.product);
      };
    return Category
}