const Promise = require('bluebird');
const UserModel = require('../models').user;
const _ = require('lodash');

function UserService() {
    const getAll = () => {
        return new Promise((resolve, reject) => {
            UserModel.findAll()
                .then(users => {
                    resolve(users);
                })
                .catch(err => {
                    reject(err);
                })
        });
    }
    const byIdWithMember = (id) => {
        return new Promise((resolve, reject) => {
          UserModel.findOne({
            where: {
              id: id
            }
          }).then(user => {
            resolve(user);
          }).catch(function (err) {
            reject(err);
          });
        });
      }
      const update = (id, obj, memberObj) => {
        let data = _.clone(_.omitBy(obj, _.isNil));
        let memberData = _.clone(_.omitBy(memberObj, _.isNil));
        const filter = {
          where: {
            id: parseInt(id)
          },
        }
        return new Promise((resolve, reject) => {
          UserModel.findOne({
            where: {
              id: parseInt(id)
            },
          })
            .then(user => {
              if (!user) reject(new Error("user tidak ditemukan"));
              if (user.member) {
                user.member.updateAttributes(memberData).then(result => {
                  user.updateAttributes(data).then((userUpdated) => {
                    resolve(userUpdated);
                  });
                });
              } else {
                user.updateAttributes(data).then((userUpdated) => {
                  resolve(userUpdated);
                });
              }
            });
        })
      };

      const remove = (id) => {
        return new Promise((resolve, reject) => {
          UserModel.destroy({
            where: {
              id: id
            }
          })
            .then(users => {
              resolve(users);
            })
            .catch(err => {
              // throw err;
              reject(err);
            });
        });
      }
    return {
        getAll,
        byIdWithMember,
        update,
        remove
    }
}

module.exports = UserService()