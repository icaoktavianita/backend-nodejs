module.exports = function(sequelize, Sequelize) {
    var Brand = sequelize.define('brand', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        },
        country: {
            type: Sequelize.STRING
        }
    })
      Brand.associate = function(models) {
        Brand.hasMany(models.product);
      };
    return Brand
}