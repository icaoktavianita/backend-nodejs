module.exports = function(sequelize, Sequelize) {
    var Body_concern = sequelize.define('body_concern', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Body_concern.associate = function(models) {
        Body_concern.hasMany(models.user_concern);
      };
    return Body_concern
}