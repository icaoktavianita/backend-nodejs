module.exports = function(sequelize, Sequelize) {
    var Skin_type = sequelize.define('skin_type', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Skin_type.associate = function(models) {
        Skin_type.hasMany(models.profile);
      };
    return Skin_type
}