module.exports = function (sequelize, Sequelize) {
    var Haircare = sequelize.define('haircare', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }
    })
    Haircare.associate = (models) => {
        Haircare.belongsTo(models.user);
        Haircare.belongsTo(models.product);
        Haircare.hasMany(models.profile);
    };
    return (
        Haircare
    )
}
