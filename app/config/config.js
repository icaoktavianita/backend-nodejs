try {
    var config = require('../../config');
  } catch (e) {
    var configError = 'Could not find config file. Please make sure `.config.js` exists on your root directory.' +
      'You can also create new config from template file `.config.js.example`.';
    throw Error(configError);
  }

function getEnv(name, defaultValue) {
    var uc = name.toUpperCase();
    var lc = name.toLowerCase();
    return process.env[uc] || process.env[lc] || defaultValue;
  }
  function getNodeEnv() {
    var env = config.env;
    return String(env).toLowerCase();
  }

  module.exports = {
      env: getEnv,
      nodeEnv: getNodeEnv
  }