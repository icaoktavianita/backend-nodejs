module.exports = function(sequelize, Sequelize) {
    var Hair_concern = sequelize.define('hair_concern', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Hair_concern.associate = function(models) {
        Hair_concern.hasMany(models.user_concern);
      };
    return Hair_concern
}