module.exports = function(sequelize, Sequelize) {
    var Skin_tone = sequelize.define('skin_tone', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Skin_tone.associate = function(models) {
        Skin_tone.hasMany(models.profile);
      };
    return Skin_tone
}