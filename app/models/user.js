module.exports = function (sequelize, Sequelize) {

    var User = sequelize.define('user', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        full_name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        username: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        email: {
            type: Sequelize.STRING,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        phone: {
            type: Sequelize.STRING,
        },
        photo: {
            type: Sequelize.STRING,
            // get() {
            //     return config.getStaticUrlUserPhoto() + this.getDataValue('photo')
            // }
        },
        birth_date: {
            type: Sequelize.DATEONLY
        },
        colored_hair: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        hijaber: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
    });
    return User;

}