const bcrypt = require('bcrypt-nodejs')

function AuthenticationService() {
    const authenticate = (email, password) => {
        let auth = User.findOne({
            where: {
                $or: [{
                    email: email
                },{
                    username: email
                }]
            },
        })
        return new Promise(function (resolve, reject){
            auth.then(function(data) {
                if(data){
                    if(bcrypt.compareSync(password, data.password())){
                        resolve(data)
                    } else {
                        reject(Error('Username atau password tidak cocok'))
                    }
                } else {
                    reject(Error('Username belum terdaftar'))
                }
            })
            .catch(function(e) {
                reject(e)
            })
        })
    }
    return {
        authenticate
    }
}

module.exports = AuthenticationService()