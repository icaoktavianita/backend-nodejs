var authController = require('../controllers/authcontroller.js');
var serviceResponse = require('../formatter/ServiceResponse')

module.exports = function (app, passport) {
    app.get('/login', authController.login)

    app.post('/signup', authController.signup);

    app.get('/dashboard', isLoggedIn, authController.dashboard);

    app.get('/logout', authController.logout);

    app.post('/signin', authController.signin);

    app.get('/users', authController.users, serviceResponse);

    app.get('/users/:user_id', authController.detail, serviceResponse)

    app.put('/users/:user_id', authController.update, serviceResponse)

    app.delete('/users/:user_id', authController.remove, serviceResponse)

    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())

            return next();

        res.redirect('/login');

    }

}