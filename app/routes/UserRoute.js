var express = require('express')
var app = express()

var multer = require('multer');

var UserController = require('../controllers/UserController');
var serviceResponse = require('../formatter/ServiceResponse')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/users')
    },
    filename: function (req, file, cb) {
      cb(null, req.params.user_id + Date.now() + '.jpg')
    }
  })
  
  var upload = multer({
    storage: storage
  });

    app.post('/signin', UserController.signin);

    app.post('/signup', UserController.signup);

    app.get('/', UserController.users, serviceResponse);
    
    app.post('/upload/:user_id', upload.single('photo'), UserController.upload, serviceResponse)

    app.get('/:user_id', UserController.detail, serviceResponse)

    app.put('/:user_id', UserController.update, serviceResponse)

    app.delete('/:user_id', UserController.remove, serviceResponse)

module.exports = app