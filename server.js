var express = require('express')
var app = express()

var passport = require('passport')
var session = require('express-session')
var bodyParser = require('body-parser')
var env = require('dotenv').load()

var cors = require('cors');

var models = require('./app/models');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors());
app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }))
app.use(passport.initialize())
app.use(passport.session())

var authRoute = require('./app/routes')(app, passport);

require('./app/config/passport/passport.js')(passport, models.user);

models.sequelize.sync().then(() => {
    console.log('Nice! Database looks fine')
}).catch((err) => {
    console.log('Something went wrong', err)
})

app.use((req, res, next) => {
    let err = new Error('Path Not Found');
    err.code = 404;
    next(err);
  });

app.use((err, req, res, next) => {
    let statusCode = err.code;
    if (statusCode >= 100 && statusCode < 600)
      res.status(statusCode);
    else
      res.status(500);
    let message = err.message;
    delete err.message;
    delete err.code;
     
    res.json({
      status: statusCode,
      message: message,
      data: err
    });
  });

app.listen(3000, (err) => {
    if(!err)
        console.log('Site is live')
    else console.log(err)
})