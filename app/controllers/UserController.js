var passport = require('passport')

var UserService = require('../service/UserService')

var exports = module.exports = {}

exports.signup = (req, res, next) => {
    passport.authenticate('local-signup', function (err, user, info) {
        if (err) { return next(err); }

        req.logIn(user, function (err) {
            if (err) {
                let err = new Error('Email already exist');
                err.code = 500;
                return next(err)
            }
            return res.json(
                {
                    id: req.user.id,
                    full_name: req.user.full_name,
                    username: req.user.username,
                    email: req.user.email,
                    phone: req.user.phone,
                    photo: req.user.photo,
                    birth_date: req.user.birth_date,
                    colored_hair: req.user.colored_hair,
                    hijaber: req.user.hijaber,
                    createdAt: req.user.createdAt
                });
        });
    })(req, res, next);
}

exports.signin = function (req, res, next) {

    passport.authenticate('local-signin', function (err, user, info) {
        if (err) { return next(err); }

        req.logIn(user, function (err) {
            if (err) {
                let err = new Error('Incorrect password or email');
                err.code = 401;
                return next(err)
            }
            return res.json(
                {
                    id: req.user.id,
                    full_name: req.user.full_name,
                    username: req.user.username,
                    email: req.user.email,
                    phone: req.user.phone,
                    photo: req.user.photo,
                    birth_date: req.user.birth_date,
                    colored_hair: req.user.colored_hair,
                    hijaber: req.user.hijaber,
                    createdAt: req.user.createdAt
                });
        });
    })(req, res, next);
}
exports.users = function (req, res, next) {
    UserService.getAll()
        .then(users => {
            req.data = users;
            next();
        })
        .catch(err => {
            next(err);
        })
}

exports.detail = function (req, res, next) {
    let id = req.params.user_id
    return UserService.byIdWithMember(id)
        .then(user => {
            if (user) {
                req.data = user;
                return next();
            }
        })
        .catch(err => {
            return next(err);
        });
}

exports.update = function (req, res, next) {
    let id = req.params.user_id
    let userObj = req.body;
    return UserService.update(id, userObj)
        .then((user) => {
            req.data = user;
            return next();
        })
        .catch(err => {
            return next(err);
        });
}

exports.remove = function (req, res, next) {
    const id = req.params.user_id
    return UserService.remove(id)
        .then((users) => {
            req.data = !!users;
            return next();
        })
        .catch(err => {
            throw err;
        });
}

exports.upload = function (req, res, next) {
    let id = req.params.user_id
    let userObj = {
        photo: req.file.filename
    };
    return UserService.update(id, userObj)
        .then((user) => {
            req.data = user;
            next();
        })
        .catch(err => {
            next(err);
        });

}

exports.dashboard = function (req, res) {

    res.render('dashboard');

}

exports.login = function (req, res) {

    res.render('login');

}