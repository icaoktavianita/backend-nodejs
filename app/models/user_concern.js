module.exports = function (sequelize, Sequelize) {
    var User_concern = sequelize.define('user_concern', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }
    })
    User_concern.associate = (models) => {
        User_concern.belongsTo(models.user);
        User_concern.belongsTo(models.skin_concern);
        User_concern.belongsTo(models.body_concern);
        User_concern.belongsTo(models.hair_concern);
    };
    return (
        User_concern
    )
}