module.exports = function (sequelize, Sequelize) {
    var Skincare = sequelize.define('skincare', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }
    })
    Skincare.associate = (models) => {
        Skincare.belongsTo(models.user);
        Skincare.belongsTo(models.product);
        Skincare.hasMany(models.profile);
    };
    return (
        Skincare
    )
}
