
module.exports = (app, passport) => {
    
    app.use('/users', require('./UserRoute'));
  
    return app;
  };  

// var authController = require('../controllers/authcontroller');
// var UserController = require('../controllers/UserController');
// var serviceResponse = require('../formatter/ServiceResponse')

// module.exports = function (app, passport) {

//     app.post('/signup', authController.signup);

//     app.get('/dashboard', isLoggedIn, authController.dashboard);

//     app.get('/logout', authController.logout);

//     app.post('/signin', authController.signin);

//     app.get('/users', authController.users, serviceResponse);

//     app.get('/users/:user_id', authController.detail, serviceResponse)

//     app.put('/users/:user_id', authController.update, serviceResponse)

//     app.delete('/users/:user_id', authController.remove, serviceResponse)

//     function isLoggedIn(req, res, next) {

//         if (req.isAuthenticated())

//             return next();

//         res.redirect('/login');

//     }

// }