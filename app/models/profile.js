module.exports = function (sequelize, Sequelize) {
    var Profile = sequelize.define('profile', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }
    })
    Profile.associate = (models) => {
        Profile.belongsTo(models.user);
        Profile.belongsTo(models.skin_type);
        Profile.belongsTo(models.skin_tone);
        Profile.belongsTo(models.skin_undertone);
        Profile.belongsTo(models.hair_type);
        Profile.belongsTo(models.skin_type);
        Profile.belongsTo(models.user_concern);
        Profile.belongsTo(models.skincare);
        Profile.belongsTo(models.bodycare);
        Profile.belongsTo(models.haircare);
    };
    return (
        Profile
    )
}
