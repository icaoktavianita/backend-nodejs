module.exports = function(sequelize, Sequelize) {
    var Hair_type = sequelize.define('hair_type', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Hair_type.associate = function(models) {
        Hair_type.hasMany(models.profile);
      };
    return Hair_type
}