module.exports = function(sequelize, Sequelize) {
    var Skin_undertone = sequelize.define('skin_undertone', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Skin_undertone.associate = function(models) {
        Skin_undertone.hasMany(models.profile);
      };
    return Skin_undertone
}