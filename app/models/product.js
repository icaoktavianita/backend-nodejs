module.exports = function (sequelize, Sequelize) {
    var Product = sequelize.define('product', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        product_name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        variant_name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: Sequelize.TEXT,
        price: Sequelize.STRING,
    })

    Product.associate = (models) => {
        Product.belongsTo(models.brand);
        Product.belongsTo(models.category);
        Product.hasMany(models.skincare)
        Product.hasMany(models.bodycare)
        Product.hasMany(models.haircare)
    };
    return (
        Product
    )
}
