module.exports = function(sequelize, Sequelize) {
    var Skin_concern = sequelize.define('skin_concern', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        }
    })
    Skin_concern.associate = function(models) {
        Skin_concern.hasMany(models.user_concern);
      };
    return Skin_concern
}