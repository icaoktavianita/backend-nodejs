module.exports = function (sequelize, Sequelize) {
    var Bodycare = sequelize.define('bodycare', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }
    })
    Bodycare.associate = (models) => {
        Bodycare.belongsTo(models.user);
        Bodycare.belongsTo(models.product);
        Bodycare.hasMany(models.profile);
    };
    return (
        Bodycare
    )
}
